import java.util.Scanner;

public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    Scanner sc = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX,playerO);

    }
    public void showWelcome(){
            System.out.println(" Welcome to Game ");
    }
    public void showTable() {
        table.showTable();
    }
    public void input() {
        while(true) {
            System.out.println("Please input Row Col : ");
            row = sc.nextInt()-1;
            col = sc.nextInt()-1;
            if(table.setRowCol(row,col)){
                break;
            }
            System.out.println("Error : table at row and col is not empty !!! ");
        }
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }
    public void newGame() {
        int rand = ((int)(Math.random()*100))%2;
        if(rand==0){
            table = new Table(playerX,playerO);
        } else {
            table = new Table(playerO,playerX);
        }


    }
    public void run() {
        this.showWelcome();
        while(true){
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            checkFinish();
            table.switchPlayer();
        }

    }
    private void checkFinish() {

    }

    public void play() {
    }
}
